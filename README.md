`dok-biblatex-iest`
===================

Modifizieren eines Biblatex-Stils
---------------------------------
Artikel über die Entstehung des Stils `iest`; verfasst für den Blog von http://texwelt.de, überarbeitet für freiesMagazin; laufend aktualisiert. Der Stil selbst ist hier gehostet: https://gitlab.com/Ekkehardt/biblatex-iest


Systeme
=======
plattformunabhängig


Technische Informationen
========================
Klasse `scrartcl`. Erstellt mit TeX Live 2016 unter Ubuntu 16.04. Folgende Pakete werden im Dokument eingebunden:

	inputenc
	fontenc
	babel
	graphicx
	csquotes
	libertine
	geometry
	enumitem
	biblatex
	listings
	doclicence
	hyperref


Rechtliche Informationen
========================
Das Dokument steht unter der **CC-BY-SA 4.0 International** Lizenz.

E. Frank Sandig, 28.09.2016
