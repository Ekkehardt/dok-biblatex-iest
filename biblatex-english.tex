% !TeX encoding = UTF-8
% !TeX spellcheck = de_DE
\documentclass[11pt,a4paper,parskip]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}

\usepackage[bottom=3.0cm,inner=2.5cm,outer=2.5cm]{geometry}
\usepackage{graphicx}
\usepackage{csquotes}
\usepackage{libertine}
\usepackage{listings}
\lstset{basicstyle={\ttfamily\small},commentstyle={},columns=fullflexible,showstringspaces=false, numbers=left,numberstyle=\tiny,stepnumber=2,numbersep=5pt,inputencoding={utf8},extendedchars=false,escapeinside=``,breaklines=true,}
\usepackage[type={CC},modifier={by-sa},version={4.0}]{doclicense}

\usepackage{enumitem}
\setitemize{topsep=0pt,parsep=0pt,itemsep=4pt}
\setenumerate{topsep=0pt,parsep=0pt,itemsep=4pt}
\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\usepackage[style=iest,backend=biber]{biblatex}
\addbibresource{links-biblatex.bib}

\usepackage{hyperref}
\hypersetup{colorlinks,citecolor=black,filecolor=black,linkcolor=black,urlcolor=black}
\urlstyle{same}

% Befehle
\newcommand{\paket}[1]{{\small\texttt{#1}}}
\renewcommand*{\refname}{Links}

% Titelseite
\author{E. Frank Sandig\thanks{\href{mailto:schriftsatz@sandig-fg.de}{schriftsatz@sandig-fg.de}}}
\title{Modifizieren eines {\protect\texttt{biblatex}}-Stils}
\subtitle{Entstehung des Stils {\protect\texttt{iest}}}
\date{\today}

\begin{document}

\maketitle
\tableofcontents
\vfill
Der vorliegende Artikel richtet sich an Nutzer von {\LaTeX}, die sich erstmals damit konfrontiert sehen, besondere Vorgaben bei der Bibliografie umsetzen zu müssen, welche sich mit Standardstilen nicht vollständig realisieren lassen. Eine frühere Version des Artikels erschien 2014 im Blog von {\TeX}welt \autocite{blog}, eine überarbeite Fassung 2015 in freiesMagazin \autocite{freiM} und auf pro-linux \autocite{prolinux}. Der vorliegende Artikel gibt den Stil \paket{biblatex-iest} in Version 1.03 wieder.
\doclicenseThis
\clearpage

\section{Ausgangslage}
Schreibt man wissenschaftliche Arbeiten oder Artikel für Fachzeitschriften, so wird dafür stets ein Literaturverzeichnis benötigt. {\LaTeX} bietet mit dem Paket \paket{biblatex} ein hervorragendes Werkzeug zu dessen Erstellung. Dabei gibt es Betreuervorgaben, Institutsvorgaben, Normen oder Vorgaben der unterschiedlichen Fachpublikationen, wie das Literaturverzeichnis im Einzelfall auszusehen hat. Für den Stil \paket{iest}, welcher im vorliegenden Artikel vorgestellt wird, gelten am Institut für Eisen- und Stahltechnologie der TU Bergakademie Freiberg (IEST) die folgenden Vorgaben:
\begin{enumerate}
	\item knapper numerischer Zitationsstil
	\item Hervorhebung des Werktitels in geeigneter Weise
	\item Sortierung des Literaturverzeichnisses nach Reihenfolge der Zitation
	\item Flattersatz in der Bibliografie
	\item Autoren-, Herausgeber- und Übersetzerangabe als Nachname, Vorname
	\item durchgehende Angabe der Vornamen als Initialen
	\item Doppelpunkt nach dem letzten Autoren-, Herausgeber- und Übersetzernamen
	\item Abkürzen der Autorenliste mit \enquote{et al.} bei mehr als drei Autoren
	\item Verlagsangabe als Verlag, Ort, Datum
\end{enumerate}

Die Punkte 1 und 2 werden bereits vom Standardstil \paket{numeric-comp} umgesetzt, vgl. \autoref{pic:nc}. Punkt 3 und 4 sowie Punkt 6 lassen sich durch die Paketoptionen \paket{sorting=none}, \paket{block=ragged} und \paket{firstinits=true} beim Laden des Paketes \paket{biblatex} erreichen\footnote{Der hier vorgestellte Stil übernimmt auch diese Funktion, siehe unten.}. Sucht man über die üblichen Suchmaschinen nach Stilen für \paket{biblatex}, so erhält man überwiegend Treffer für das ältere Bib{\TeX}-System. Diese beiden Systeme sind jedoch, was die Stildateien angeht, nicht kompatibel. Obwohl die Vorgaben für des IEST keineswegs außergewöhnlich sind, war zu Beginn der Arbeit am Stil \paket{iest} kein fertiger Stil im Internet verfügbar, welcher die obigen Vorgaben vollständig umgesetzt hätte. Dieser Umstand gab den Ausschlag für die Erstellung eines neuen Stils. \paket{iest} basiert auf \paket{numeric-comp} und lädt diesen Stil, womit alle Eintragstypen und Felder, welche der Standardstil unterstützt, auch von \paket{iest} unterstützt werden.

Für einzelne Änderungen, die nur in einem Dokument benötigt werden, lohnt sich die Erstellung eines eigenen Stiles nicht. In diesem Fall kann man die Änderung in der Präambel des Dokumentes einfügen. Ein eigener \paket{biblatex}-Stil lohnt sich vor allem, wenn man mehrere Änderungen hat, man diese immer wieder benötigt oder wenn der Bibliografiestil von einer Gruppe von Autoren verwendet werden soll (z.\,B. alle Studenten eines Betreuers, ein ganzes Institut, alle Autoren bei einer bestimmten Zeitschrift, alle Wissenschaftler in einem Projekt, alle Projekte bei einem Träger usw.).

\begin{center}
	\fbox{\includegraphics[width=0.90\linewidth]{numeric-comp}}
	\captionof{figure}{Beispiel zum Stil \paket{numeric-comp}}
	\label{pic:nc}
\end{center}

% % % % % % % % % % % % % % % % % % % % % % % % % %

\section{Vorteile von {\protect\texttt{biblatex}}}
Die Verwendung des moderneren Pakets \paket{biblatex} bietet gegenüber dem älteren Bib{\TeX}-System eine Reihe von Vorteilen; beim Einsatz selbst wie auch bei der Erstellung eigener Stile. So unterstützt \paket{biblatex} mehr Felder je Eintragstyp. Darunter sind z.\,B. \paket{redactor}, \paket{shorthand}, \paket{subtitle}, \paket{bookauthor}, \paket{booktitle} und \paket{origlanguage/-location/-title/-year}. Ebenso existieren bei \paket{biblatex} von Haus aus mehr Eintragstypen als bei Bib{\TeX}, z.\,B. \paket{@collection}, \paket{@online}, \paket{@patent} und \paket{@periodical}. Darüber hinaus können relativ einfach eigene Eintragstypen definiert werden.

Viele Einstellungen die Bibliografie betreffend können bereits beim Laden des Pakets in Form von Paketoptionen übergeben werden. Darunter sind das Schema zur Sortierung der Bibliografie, Block- oder Flattersatz im Literaturverzeichnis, Verwendung bibliografischer Abkürzungen, die maximale Anzahl der ausgegebenen Namen, Angabe von Seitenzahlen der Zitate in der Bibliografie, Ausgabe von ISBN, DOI, URL, ePrint und einiges mehr. Verwendet man an Stelle von Bib{\TeX} den moderneren Bibliografieprozessor \paket{biber} zur Sortierung, was bei \paket{biblatex} voreingestellt ist, so kann man eigene Sortierschemata definieren, z.\,B. \paket{name-title-year}, und erhält darüber hinaus UTF8-Unterstützung für die eigene Literaturdatenbank, welche einen besonderen Vorteil darstellt.

Es ist möglich, unterschiedliche Zitations- und Bibliografiestile unabhängig voneinander zu laden (Paketoptionen \paket{citestyle} und \paket{bibstyle}). \paket{biblatex} bietet zahlreiche unterschiedliche Zitierbefehle, darunter \lstinline|\autocite{}|. Das Verhalten dieses Befehls lässt sich global über eine Paketoption einstellen. Optional bietet \paket{biblatex} auch \paket{natbib}-Kompatibilität. Das Paket \paket{biblatex} unterstützt unterteilte Bibliographien, mehrere Bibliographien innerhalb eines Dokuments und separate Listen bibliographischer Abkürzungen. Bei all dem ist \paket{biblatex} bezüglich der Datenbank abwärtskompatibel, so dass man alle alten \paket{.bib}-Dateien weiterhin verwenden kann. Wie man \paket{biblatex} in einem Dokument verwendet, ist dem Beispiel unter \autocite{bsp} zu entnehmen.

Man kann Bib{\TeX} zu Gute halten, dass bereits sehr viele fertige Stile existieren, da das System schon seit 1985 existiert. Dennoch werden in {\LaTeX}-Foren täglich Fragen nach der Umsetzung besonderer Vorgaben, welche über vorhandene Stile hinausgehen, gestellt. Eigene Stile für das ältere System können mit \paket{makebst} nach einem Frage-Antwort-Schema erstellt werden. Jedoch lassen sich damit, trotz des relativ großen Zeitaufwands, Vorgaben häufig nicht vollständig umsetzen. Editiert man einen \paket{.bst}-Stil selbst, so muss man dazu {Bib\TeX}-Funktionen verwenden, welche quasi eine eigene, keineswegs triviale Sprache darstellen, vgl. die Dokumentation von {Bib\TeX} \autocite{beast}.

Demgegenüber greift \paket{biblatex} lediglich optional für die Sortierung der Einträge auf {Bib\TeX} zurück. Die Formatierung des Literaturverzeichnisses und der Zitate erfolgt jedoch über {\LaTeX}-Befehle bzw. Makros. Diese Vorgehensweise ist für den erfahrenen \LaTeX-Anwender intuitiver, transparenter und effizienter als die {Bib\TeX}-Programmierung und darüber hinaus der Grund dafür, dass einzelne Änderungen in der Präambel des betreffenden Dokuments angegeben werden können. Die besondere Effizienz von \paket{biblatex} ist u.\,a. darin begründet, dass man bestehende Stile laden und einzelne Teile umdefinieren kann. Dies führt dazu, dass der Stil \paket{iest} mit wenigen Zeilen Quelltext auskommt. Aufgrund der Funktionsweise sowie einer Vielzahl an Optionen ist \paket{biblatex} weitaus flexibler als das {Bib\TeX}-System.

\section{Informationsquellen}
Sucht man im Internet nach Informationen, wie \paket{biblatex}-Stile zu erstellen und modifizieren sind, so erhält man viele über Foren verstreute Informationsschnipsel. Zwei Quellen liefern umfassendere Informationen. Zum einen ist das die hervorragende Paketdokumentation von Philipp Lehman mit Philip Kime, Audrey Boruvka und Joseph Wright. Die jeweils aktuelle englische Version findet man auf CTAN \autocite{Lehm2014}. Die lokal mit TeX~Live installierte Version kann man auf der Kommandozeile mit dem Befehl \paket{texdoc biblatex} aufrufen. Zu der Frage, wo man die Dokumentation findet, gibt es eine Anwenderfrage mit Antworten auf der Seite der {\TeX}welt \autocite{tw6725}. Die Dokumentation ist umfassend, weshalb sie ob der vielen Möglichkeiten von \paket{biblatex} sehr umfangreich gehalten ist. Es dauert relativ lange, darin alle nötigen Informationen zu den eigenen Modifikationen zusammenzusuchen. Dennoch sollte sie als offizielle Paketdokumentation im Zweifelsfall die Quelle der Wahl sein.

Da bei \LaTeX, wie so oft im Leben, der Grundsatz \textsc{exemplum docet} gilt, ist für ein schnelles Vorankommen beim Bau des eigenen Stils Dominik Waßenhovens Artikel aus der DANTE-Zeitschrift \enquote{Die {\TeX}nische Komödie} 4/08 \autocite{Wassenhoven2008} sehr hilfreich. Der Autor erläutert darin die Vorgehensweise am Beispiel von Anforderungen für eine geisteswissenschaftliche Publikation. Mit gewissen Modifikationen ist der Artikel auf andere Fachgebiete ebenso anwendbar. Die Vorgehensweise bleibt gleich, lediglich die Befehle zur Formatierung unterscheiden sich je nach Anforderungen. Waßenhovens Artikel geht über den vorliegenden dahingehend hinaus, dass auch auf Änderungen in der Darstellung der Werktitel u.\,ä. eingegangen wird, was mit einem etwas größeren Änderungsaufwand verbunden ist. Teil 1 des Artikels aus Ausgabe 2/08 \autocite{Wassenhoven2008a} geht auf die Grundlagen der Arbeit mit \paket{biblatex} ein.

Eine weiteren Einstieg in \paket{biblatex} bietet der Blogartikel \enquote{Hilfe zu \paket{biblatex}} von Marco Daniel \autocite{marco}.

\section{Lösungsansätze}
Ein vollständiger Stil für \paket{biblatex} besteht mindestens aus zwei ASCII-Dateien. Eine \paket{*.cbx}-Datei ist für das Erscheinungsbild der Zitate im Text verantwortlich. In einer Datei \paket{*.bbx} wird die Formatierung der Quellenangaben im Literaturverzeichnis festgelegt. Darüber hinaus ist es möglich, in einer \paket{*.lbx}-Datei sprachabhängige Einstellungen in Verbindung mit einem Sprachpaket wie \paket{babel} oder \paket{polyglossia} zu treffen. Man spart sehr viel Arbeit, wenn man einen eigenen Stil nicht von Null an neu schreibt, sondern einen vorhandenen Stil den eigenen Wünschen oder Vorgaben anpasst. Neben den mitgelieferten Standardstilen gibt es eine Reihe spezieller Stile unter \autocite{topic}. Für den Umgang mit Änderungen gibt es die nachfolgend erläuterten unterschiedlichen Philosophien.

\subsection{Einen Stil kopieren und ändern}
Man speichert die vorhandenen Stildateien unter neuem Namen und ändert \\ \lstinline|\ProvidesFile{stilname.cbx}| bzw. \lstinline|\ProvidesFile{stilname.bbx}| entsprechend ab. Dann beginnt man, die Definitionen den eigenen Wünschen anzupassen.

\enlargethispage{2\baselineskip}
Dieses Vorgehen ist von Vorteil, wenn man nicht wünscht, dass Änderungen am generischen Stil in den Eigenen vererbt werden und wenn man sehr viel ändern möchte. Ein Nachteil ist, dass man sehr große und unübersichtliche Stildateien erhält bzw. die eigenen Änderungen für Dritte schwerer nachvollziehbar sind.

\subsection{Einen Stil laden und nötiges umdefinieren}
Man legt neue Stildateien mit dem gewünschten Namen an, lädt mit \\ \lstinline|\RequireCitationStyle{vorhandener-stil}| bzw. \\ \lstinline|\RequireBibliographyStyle{vorhandener-stil}| einen vorhandenen Stil und definiert das Gewünschte um.

Man erhält dabei, vor allem bei wenigen Änderungen, sehr übersichtliche Stildateien. Ein möglicher Nachteil ist die Vererbung von Änderungen am vorhandenen Stil in den eigenen Stil.

Diese Vorgehensweise wurde auf Grund der wenigen nötigen Änderungen für das Erstellen des Stils \paket{iest} gewählt. Der Name \paket{iest} ist die Abkürzung für Institut für Eisen- und Stahltechnologie, an welchem der hier vorgestellte Stil entstand. Es handelt sich um einen Arbeitstitel, welcher, wie so oft, nicht mehr geändert wurde.

\subsection{Befehle und ihre Bedeutung}
\subsubsection*{Die Datei \protect\texttt{iest.cbx}}
\lstinline|\ProvidesFile{iest.cbx}| \\ Bekanntmachen der Stildatei für die Zitation.

\lstinline|\RequireCitationStyle{numeric-comp}| \\ Laden des generischen Zitationsstils; hier handelt es sich um den Standardstil \paket{numeric-comp}.

\lstinline|\endinput| \\ Ende der Stildatei.

\subsubsection*{Die Datei \protect\texttt{iest.bbx}}
\lstinline|\ProvidesFile{iest.bbx}| \\ Bekanntmachen der Stildatei für die Quellenangaben.

\lstinline|\RequireBibliographyStyle{numeric-comp}| \\ Laden des zu Grunde liegenden Bibliografiestils; hier handelt es sich um den Standardstil \paket{numeric-comp}.

\lstinline|\ExecuteBibliographyOptions{giveninits=true,sorting=none,block=ragged}| \\
Setzen von \paket{biblatex}-Optionen, welche bewirken, dass der Vorname abgekürzt, die Bibliografie nach der Zitation sortiert und das Literaturverzeichnis im Flattersatz gesetzt wird.

\lstinline|\DeclareNameAlias{default}{last-first}| \\
Das Standard-Namensformat wird auf Nachname, Vorname gesetzt. Somit werden alle in der Bibliografie auftauchenden Namen in dieser Form ausgegeben.

\lstinline|\renewcommand*{\labelnamepunct}{\addcolon\addspace}| \\ \lstinline|\labelnamepunct| enthält die Zeichenfolge, mit der die Namensangabe im Literaturverzeichnis abgeschlossen wird. Bei \paket{numeric-comp} ist das ein Punkt und ein Leerzeichen. Hier wird die Variable mit einem Doppelpunkt (\lstinline|\addcolon|) und einem Leerzeichen (\lstinline|\addspace|) neu belegt.

\lstinline|\DefineBibliographyStrings{ngerman}{andothers={et\addabbrvspace| \textrightarrow \newline
\mbox{~~~} \lstinline|al\adddot}}| \\
Der in \paket{numeric-comp}, auf Grund der dort geladenen Definition in \paket{german.lbx}, mit \enquote{u.\,a.} belegte String für die Abkürzung bei mehreren Autoren wird mit \enquote{et al.} neu belegt.\\

\begin{lstlisting}
	\renewbibmacro*{publisher+location+date}{%
	  \printlist{publisher}%
	  \setunit*{\addcomma\space}
	  \printlist{location}%
	  \setunit*{\addcomma\space}
	  \usebibmacro{date}%
	  \newunit}
\end{lstlisting}\vspace{-1\baselineskip}
Das für die Ausgabe von Verlag, Ort und Datum zuständige Makro wird so umdefiniert, dass die gewünschte Reihenfolge erreicht wird. Die Elemente der Liste werden durch Kommata getrennt (Zeilen drei und fünf).

\lstinline|\endinput| \\ Ende der Stildatei. 

\section{Ergebnis}
Beim Stil \paket{iest} gibt es gegenüber \paket{numeric-comp} keine Änderungen, was den Zitationsstil angeht. Dennoch ist es von Vorteil, eine \paket{*.cbx}-Datei anzulegen. Dadurch kann der Stil durch die Paketoption \paket{style=iest} eingebunden werden, es ist keine getrennte Angabe von \paket{bibstyle} und \paket{citestyle} notwendig. Die Option, bspw. den ausführlicheren Zitationsstil über \paket{citestyle=numeric} zu laden, bleibt davon unberührt. Während in \paket{numeric-comp} und damit auch in \paket{iest} aufeinander folgende Quellennummern (1,2,3) in der Form 1--3 abgekürzt werden, listet \paket{numeric} die Quellen vollständig auf.

\subsubsection*{Die Datei \protect\texttt{iest.cbx}}
\lstinputlisting{iest.cbx.lst}

\subsubsection*{Die Datei \protect\texttt{iest.bbx}}
\lstinputlisting{iest.bbx.lst}

\subsubsection*{Die Beispieldatei \protect\texttt{iest.tex}}
\lstinputlisting{iest.tex.lst}

\subsubsection*{Die Beispielliteratur \protect\texttt{quellen-iest.bib}}
\lstinputlisting{quellen-iest.bib.lst}

\subsubsection*{Das fertig compilierte Beispiel}
\begin{center}
\fbox{\includegraphics[width=0.90\linewidth]{iest}}
\captionof{figure}{Beispiel zum Stil \paket{iest}}
\label{pic:beispiel}
\end{center}

\section{Bezug und Installation des Stils \protect\texttt{iest}}
Den aktuellen Stand des Stils \paket{iest} findet man, inklusive Beispiel, stets auf gitlab \autocite{gitlab}. Die aktuelle Version 1.03 vom 30.09.2016 steht dort als Quelltextarchiv zum Download bereit. Neben den \paket{.cbx}- und \paket{.bbx}-Dateien findet sich dort ebenso ein Anwendungsbeispiel mit einer kleinen Literaturdatenbank. Die Kommentarzeilen (beginnend mit \%) werden von {\LaTeX} nicht interpretiert. Sie dienen dazu, Informationen über die Dateien an Anwender oder andere Entwickler weiterzugeben. Zur Problemstellung, wo eigene Makros u.\,ä. zu speichern sind, gibt es eine Frage auf der {\TeX}welt \autocite{tw3528}. Die Installation für den aktuell angemeldeten Benutzer in TeX~Live unter Windows, Linux und Mac\-OS besteht aus dem folgenden Schritt:

\begin{enumerate}
	\item Kopieren der Dateien \paket{iest.cbx} und \paket{iest.bbx} nach \\ \lstinline|~/texmf/tex/latex/biblatex-iest|	
\end{enumerate}
In MacTeX unter MacOS läuft die Installation des Stils wie folgt ab:
\begin{enumerate}
	\item Kopieren der Dateien \paket{iest.cbx} und \paket{iest.bbx} nach \\ \lstinline|~/Library/texmf/tex/latex/biblatex-iest|
\end{enumerate}
In MikTeX unter Windows gilt folgende Vorgehensweise: 
\begin{enumerate}
	\item Anlegen eines privaten Baumes, z.\,B. unter \lstinline|~/texmf| (muss außerhalb des Verzeichnisses der MikTeX-Installation liegen)
	\item Kopieren der Dateien \paket{iest.cbx} und \paket{iest.bbx} nach \\ \lstinline|~/texmf/tex/latex/biblatex-iest|
	\item Registrieren des privaten Baumes als \enquote{Root}
		\begin{enumerate}
			\item MikTeX Settings starten
			\item Reiter \enquote{Roots}
			\item Schaltfläche \enquote{Add..}
			\item obigen Ordner auswählen
			\item ggf. Suchreihenfolge anpassen
			\item Schaltfläche \enquote{Übernehmen}
		\end{enumerate}
\end{enumerate}
In allen Fällen ist \lstinline|~/| das Heimatverzeichnis des aktuell angemeldeten Benutzers.

\section{Support}
Für Rückfragen steht die E-Mail-Adresse \href{mailto:schriftsatz@sandig-fg.de}{schriftsatz@sandig-fg.de} zur Verfügung. Fehlermeldungen, Problemberichte, Verbesserungsvorschläge u.\,ä. können über den Issue-Tracker unter \url{https://gitlab.com/Ekkehardt} eingereicht werden.

\printbibliography[heading=bibintoc]

%\paragraph{Zum Autor}
%E. Frank Sandig studiert Werkstoffwissenschaft und -technologie an der TU Bergakademie Freiberg. Er beschäftigt sich seit 1999 mit Linux und Open Source und seit 2007 mit \LaTeX. Derzeit entsteht seine Diplomarbeit -- natürlich in \LaTeX.

%\section*{Dank}
%Ich danke Johannes Böttcher für fachlichen Rat und das Korrekturlesen des Artikels.

\end{document}
